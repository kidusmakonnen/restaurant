package com.kidus.androidapps.restaurant2;

import java.util.UUID;

/**
 * Created by Kidus on .
 */
public class Order {
    private UUID mOrderUUID;
    private Items mItem;
    private int mOrderAmount;

    public UUID getOrderUUID() {
        return mOrderUUID;
    }

    public Order(Items item, int orderAmount) {
        mOrderUUID = UUID.randomUUID();

        mItem = item;
        mOrderAmount = orderAmount;
    }

    public String getOrderItemName() {
        return mItem.getName();
    }

    public double getOrderItemPrice() {
        return mItem.getItemPrice();
    }

    public String getOrderItemPriceFormatted() {
        return mItem.getFormattedItemPrice();
    }

    public int getOrderAmount() {
        return mOrderAmount;
    }

    public Items getOrderItem() {
        return mItem;
    }

}
