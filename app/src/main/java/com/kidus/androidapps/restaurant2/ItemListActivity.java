package com.kidus.androidapps.restaurant2;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ImageView;

public class ItemListActivity extends AppCompatActivity {
    private CollapsingToolbarLayout mCollapsingToolbarLayout;
    private ImageView mImageView;

    private ItemListAdapter mItemListAdapter;
    private LinearLayoutManager mLinearLayoutManager;
    private RecyclerView mRecyclerView;

    private TabLayout mTabLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_item_list);

        Category selectedCategory = (Category) getIntent().getSerializableExtra("SELECTED_CATEGORY");
        mCollapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.items_collapsing_toolbar_layout);
        mCollapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));

        final Toolbar toolbar = (Toolbar) findViewById(R.id.category_toolbar_toolbar);
        toolbar.setTitle(selectedCategory.getCategoryNameString());
        setSupportActionBar(toolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mTabLayout = (TabLayout) findViewById(R.id.tab_lists);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager_lists);
        final TabsAdapter tabsAdapter = new TabsAdapter(getSupportFragmentManager(),
                new ItemListFragment(),
                "List of " + selectedCategory.getCategoryNameString()
        );
        viewPager.setAdapter(tabsAdapter);

        mTabLayout.setupWithViewPager(viewPager);


        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                OrderFragment.refresh();//not sure if this is appropriate :|
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        mImageView = (ImageView) findViewById(R.id.category_toolbar_image_view);
        mImageView.setImageResource(selectedCategory.getCategoryImageResourceId());


        Bitmap categoryPicture = ((BitmapDrawable) mImageView.getDrawable()).getBitmap();
        Palette.from(categoryPicture).generate(new Palette.PaletteAsyncListener() {
            @Override
            public void onGenerated(Palette palette) {
                int dark = getResources().getColor(android.R.color.black);
                int grey = getResources().getColor(R.color.dark_grey);
                mCollapsingToolbarLayout.setContentScrimColor(palette.getDarkVibrantColor(grey));
                mCollapsingToolbarLayout.setStatusBarScrimColor(palette.getMutedColor(dark));

                mTabLayout.setBackgroundColor(palette.getDarkVibrantColor(grey));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    finishAfterTransition();
                } else {
                    finish();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
