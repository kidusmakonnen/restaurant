package com.kidus.androidapps.restaurant2;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Kidus on .
 */
public class OrderList {
    private static ArrayList<Order> mOrders = new ArrayList<>();

    private static Tip sTip;

    private static int sTipPercentage;
    private static int sTipIncrementValue;
    private static int sMinTipAmount, sMaxTipAmount;

    public static void incrementPercentage() {
        if (getTipPercentageInt() < sMaxTipAmount) {
            sTip.setPercentage(sTip.getPercentageInt() + sTipIncrementValue);
        }
    }

    public static boolean isEmpty() {
        return mOrders.isEmpty();
    }

    public static void updateTip() {
        sTip.setAmount(getTotalPrice());
    }

    public static void decrementPercentage() {
        if (getTipPercentageInt() > sMinTipAmount) {
            sTip.setPercentage(sTip.getPercentageInt() - sTipIncrementValue);
        }
    }

    public static double getTipIncrementValue() {
        return sTipIncrementValue;
    }

    static {
        sMinTipAmount = 0;
        sMaxTipAmount = 45;
        sTipPercentage = 15;
        sTipIncrementValue = 5;
        sTip = new Tip(getTotalPrice(), sTipPercentage);
    }

    public static void setTipIncrementValue(int tipIncrementValue) {
        sTipIncrementValue = tipIncrementValue;
    }

    public static Tip getTip() {
        return sTip;
    }

    public static int getTipPercentageInt() {
        return sTip.getPercentageInt();
    }

    public static double getTipPercentage() {
        return sTip.getPercentage();
    }

    public static void setTipPercentage(double tipPercentage) {
        sTip.setPercentage(tipPercentage);
    }

    public static double getCalculatedTipAmount() {
        return sTip.getCalculatedTipAmount();
    }

    public static double getTotalAmountWithTip() {
        return sTip.getTotalAmountWithTip();
    }

    public static String getFormattedPercentage() {
        return sTip.getFormattedPercentage();
    }

    public static void add(Order order) {
        mOrders.add(order);
        sTip.setAmount(getTotalPrice());
    }

    public static ArrayList<Order> getOrders() {
        return mOrders;
    }

    public static void remove(UUID orderUuid) {
        for (Order i : mOrders) {
            if (i.getOrderUUID() == orderUuid) {
                mOrders.remove(i);

            }
        }
    }

    public static void remove(int position) {
        mOrders.remove(position);
    }

    public static int size() {
        return mOrders.size();
    }

    public static Order getOrder(int position) {
        return mOrders.get(position);
    }

    public static double getTotalPrice() {
        double v = 0.0;
        for (Order i : mOrders) {
            v += i.getOrderItemPrice() * i.getOrderAmount();
        }
        return v;
    }

    public static String getFormattedTotalPrice() {
        return Items.toCurrencyFormat(getTotalPrice());
    }
}
