package com.kidus.androidapps.restaurant2;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Created by Kidus on .
 */
public class ItemListFragment extends Fragment {
    private CollapsingToolbarLayout mCollapsingToolbarLayout;
    private ImageView mImageView;

    private ItemListAdapter mItemListAdapter;
    private LinearLayoutManager mLinearLayoutManager;
    private RecyclerView mRecyclerView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRecyclerView = (RecyclerView) inflater.inflate(
                R.layout.fragment_item_list, container, false
        );

        Category selectedCategory = (Category) getActivity().getIntent().getSerializableExtra("SELECTED_CATEGORY");




        mLinearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mItemListAdapter = new ItemListAdapter(getActivity(), selectedCategory.getCategoryItems());
        mItemListAdapter.setListener(new ItemListAdapter.Listener() {
            @Override
            public void onClick(Items item, View view) {
                Intent intent = new Intent(getActivity(), ItemDetailsActivity.class);
                intent.putExtra("SELECTED_ITEM", item);
                Pair<View, String> imagePair = Pair.create(view, "transition_item_details_image");

                ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        getActivity(),
                        imagePair
                );
                ActivityCompat.startActivity(getActivity(), intent, optionsCompat.toBundle());
//                startActivity(intent);
            }
        });

//        mItemListAdapter = new ItemListAdapter(this, selectedCategory.getCategoryItems(),
//                Palette.generate(categoryPicture).getDarkMutedColor(getResources().getColor(R.color.colorPrimary)),
//                Palette.generate(categoryPicture).getLightMutedColor(getResources().getColor(android.R.color.primary_text_dark)));
        //mItemListAdapter.setHasStableIds(true);
        mRecyclerView.setAdapter(mItemListAdapter);

        return mRecyclerView;
    }
}
