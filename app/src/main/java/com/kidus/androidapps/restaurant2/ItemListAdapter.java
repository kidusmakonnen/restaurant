package com.kidus.androidapps.restaurant2;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by Kidus on .
 */
public class ItemListAdapter extends RecyclerView.Adapter<ItemListAdapter.ViewHolder> {

    private Items[] mSelectedItems;
    private Context mContext;

    Listener mListener;

    public void setListener(Listener listener) {
        this.mListener = listener;
    }

    public interface Listener {
        void onClick(Items item, View view);
    }


    public ItemListAdapter(Context context, Items[] items) {
        this.mSelectedItems = items;
        this.mContext = context;
    }

    @Override
    public ItemListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_card_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ItemListAdapter.ViewHolder holder, final int position) {
        Picasso.with(mContext).load(mSelectedItems[position].getItemImageId()).into(holder.itemImage);
        holder.itemDetails.setText(mContext.getResources().getText(mSelectedItems[position].getItemDescriptionId()));
        holder.itemPrice.setText(String.valueOf(mSelectedItems[position].getFormattedItemPrice()));
        holder.itemTitle.setText(mSelectedItems[position].getName());

        //for test only, add these attributes to the objects before runtime...your fucking up the performance
        Bitmap itemImage = ((BitmapDrawable) mContext.getResources().getDrawable(
                mSelectedItems[position].getItemImageId()
        )).getBitmap();
        int backgroundColor = mSelectedItems[position].getItemDescriptionBackgroundColor();
        //int foregroundColor = Palette.generate(itemImage).getLightMutedColor(mContext.getResources().getColor(android.R.color.primary_text_dark));

        holder.itemDescriptionLayout.setBackgroundColor(backgroundColor);

        holder.itemDetailsCoverLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onClick(mSelectedItems[position], holder.itemImage);
                }
            }
        });

//        holder.itemTitle.setTextColor(foregroundColor);
//        holder.itemDetails.setTextColor(foregroundColor);
//        holder.itemPrice.setTextColor(foregroundColor);
    }

    @Override
    public int getItemCount() {
        return mSelectedItems.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView itemImage;
        public TextView itemDetails, itemPrice, itemTitle;
        public LinearLayout itemDetailsCoverLayout;
        public RelativeLayout itemDescriptionLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            itemImage = (ImageView) itemView.findViewById(R.id.item_image_card_image_view);
            itemDetails = (TextView) itemView.findViewById(R.id.item_details_card_text_view);
            itemPrice = (TextView) itemView.findViewById(R.id.item_price_card_text_view);
            itemTitle = (TextView) itemView.findViewById(R.id.item_title_card_text_view);
            itemDescriptionLayout = (RelativeLayout) itemView.findViewById(R.id.item_description_linear_layout);
            itemDetailsCoverLayout = (LinearLayout) itemView.findViewById(R.id.item_description_cover_linear_layout);
        }
    }
}
