package com.kidus.androidapps.restaurant2;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Kidus on .
 */
public class TabsAdapter extends FragmentStatePagerAdapter {
    final int NUM_OF_TABS = 2;
    Fragment mSelectedFragment;
    String mSelectedTabTitle;
    public TabsAdapter(FragmentManager fm, Fragment selectedFragment, String selectedTabTitle) {
        super(fm);
        this.mSelectedFragment = selectedFragment;
        this.mSelectedTabTitle = selectedTabTitle;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return mSelectedFragment;
            case 1:
                return new OrderFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NUM_OF_TABS;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mSelectedTabTitle;
            case 1:
                return "Orders";
            default:
                return null;
        }
    }
}
