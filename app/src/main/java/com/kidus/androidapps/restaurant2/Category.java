package com.kidus.androidapps.restaurant2;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by Kidus on .
 */
public class Category implements Serializable {
    private UUID mCategoryId;
    private int mCategoryNameResourceId;
    private int mCategoryImageResourceId;
    private Items[] mCategoryItems;


    public static final Category[] categories = {
            new Category(R.string.pizza_category_name, R.drawable.pizza, Items.pizzaItems),
            new Category(R.string.burger_category_name, R.drawable.burgers, Items.burgerItems),
            new Category(R.string.spaghetti_category_name, R.drawable.spaghetti, Items.spaghettiItems),
            new Category(R.string.ethiopian_food_category_name, R.drawable.ethiopian, Items.ethiopianFoodItems),
            new Category(R.string.soft_drinks_category_name, R.drawable.soft, Items.softDrinkItems),
            new Category(R.string.alcoholic_drinks_category_name, R.drawable.alcohol, Items.alcoholicDrinkItems)
    };

    public Category(int name, int imageResourceId, Items[] categoryItems) {
        this.mCategoryId = UUID.randomUUID();
        this.mCategoryNameResourceId = name;
        this.mCategoryImageResourceId = imageResourceId;
        this.mCategoryItems = categoryItems;
    }

    public UUID getCategoryId() {
        return mCategoryId;
    }

    public String getCategoryNameString() {
        return MainActivity.getContext().getString(mCategoryNameResourceId);
    }

    public int getCategoryNameResourceId() {
        return mCategoryNameResourceId;
    }

    public int getCategoryImageResourceId() {
        return mCategoryImageResourceId;
    }

    public Items[] getCategoryItems() {
        return this.mCategoryItems;
    }
}
