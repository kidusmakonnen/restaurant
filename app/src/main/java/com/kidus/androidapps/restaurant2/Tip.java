package com.kidus.androidapps.restaurant2;

/**
 * Created by Kidus on .
 */
public class Tip {
    private double amount;
    private double percentage; //0.0 to 1.0

    public Tip(double amount, double percentage) {
        this.amount = amount;
        this.percentage = percentage;
    }

    public Tip(double amount, int percentage) {
        this.amount = amount;
        this.percentage = percentage / 100.0;
    }

    public static String getFormattedPercentage(double v) {
        if (v < 0.0 || v > 1.0) {
            throw new ArithmeticException("Percentage value should be between 0.0 and 1.0");
        } else {
            String s = String.format("%d%%", (int) (v * 100));
            return s;
        }
    }

    public double getAmount() {
        return amount;
    }

    public double getPercentage() {
        return percentage;
    }

    public String getFormattedPercentage() {
        return getFormattedPercentage(percentage);
    }

    public double getCalculatedTipAmount() {
        return amount * percentage;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage / 100.0;
    }

    public double getTotalAmountWithTip() {
        return amount + getCalculatedTipAmount();

    }

    public int getPercentageInt() {
        return (int) (percentage * 100);
    }

}
