package com.kidus.androidapps.restaurant2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Kidus on .
 */
public class OrderFragment extends Fragment {

    RecyclerView mRecyclerView;
    LinearLayoutManager mLinearLayoutManager;
    private static OrderListAdapter sOrderListAdapter;
    private OrderListAdapter mOrderListAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mRecyclerView = (RecyclerView) inflater.inflate(
                R.layout.fragment_order,
                container,
                false
        );

        mLinearLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        mOrderListAdapter = new OrderListAdapter(getContext());
        sOrderListAdapter = mOrderListAdapter;

        mOrderListAdapter.setListener(new OrderListAdapter.Listener() {
            @Override
            public void onClick(int position) {
                mOrderListAdapter.removeItem(position);
            }
        });

        mRecyclerView.setAdapter(mOrderListAdapter);

//        initSwipe();

        return mRecyclerView;
    }

//    private void initSwipe() {
//        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0,
//                ItemTouchHelper.LEFT) {
//            @Override
//            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
//                return false;
//            }
//
//            @Override
//            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
//                int position = viewHolder.getAdapterPosition();
//
//                if (direction == ItemTouchHelper.LEFT) {
//                    mOrderListAdapter.removeItem(position);
//                }
//            }
//        };
//        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
//        itemTouchHelper.attachToRecyclerView(mRecyclerView);
//    }

    public static void refresh() {
        sOrderListAdapter.notifyDataSetChanged();
    }
}
