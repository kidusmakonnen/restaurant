package com.kidus.androidapps.restaurant2;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Created by Kidus on .
 */
public class MainFragment extends Fragment {

    RecyclerView mRecyclerView;
    private StaggeredGridLayoutManager mLayoutManager;
    private CategoryListAdapter mCategoryListAdapter;
    private ImageView mCategoryImageView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRecyclerView = (RecyclerView) inflater.inflate(
                R.layout.fragment_main, container, false
        );

        mLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mCategoryListAdapter = new CategoryListAdapter(getActivity());
        mCategoryListAdapter.setListener(new CategoryListAdapter.Listener() {
            @Override
            public void onClick(int Position, View view) {
                //View statusBar = findViewById(android.R.id.statusBarBackground);
                mCategoryImageView = (ImageView) view;
//                mCategoryImageView = (ImageView) view.findViewById(R.id.category_image_view);
                //Toast.makeText(MainActivity.this, "Clicked on " + Position, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getActivity(), ItemListActivity.class);
                intent.putExtra("SELECTED_CATEGORY", Category.categories[Position]);
                Pair<View, String> categoryImage = Pair.create((View) mCategoryImageView, "transition_category_image");
                //Pair<View, String> statusBarPair = Pair.create(statusBar, Window.STATUS_BAR_BACKGROUND_TRANSITION_NAME);
                //Pair<View, String> toolbarPair = Pair.create((View) toolbar, "transition_toolbar");

                ActivityOptionsCompat activityCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        getActivity(),
                        categoryImage
                        //statusBarPair
                        //toolbarPair
                );
                ActivityCompat.startActivity(getActivity(), intent, activityCompat.toBundle());
//                startActivity(intent);
            }
        });
        mRecyclerView.setAdapter(mCategoryListAdapter);


        return mRecyclerView;
    }
}
