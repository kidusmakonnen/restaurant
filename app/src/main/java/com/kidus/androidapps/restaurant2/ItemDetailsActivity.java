package com.kidus.androidapps.restaurant2;

import android.animation.Animator;
import android.content.res.ColorStateList;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Transition;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class ItemDetailsActivity extends AppCompatActivity {

    private ImageView mItemImageView;
    private Toolbar mItemToolbar;
    private FloatingActionButton mFab;

    private TextView mItemPrice, mItemDescription, mItemOrderPrice, mItemOrderTotalPrice;
    private Button mOrderButton;

    private LinearLayout mOrderRelativeLayout;
    private RelativeLayout mItemDescriptionRelativeLayout;
    private boolean mOrderLayoutVisible = false;

    private int orderAmount = 1;//temporary, make a separate class later...don't forget

    private Items selectedItem;
//    private RelativeLayout mItemDescriptionRelativeLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_item_details);

        selectedItem = (Items) getIntent().getSerializableExtra("SELECTED_ITEM");

        mItemImageView = (ImageView) findViewById(R.id.item_detail_image_image_view);
//        mItemTitleTextView = (TextView) findViewById(R.id.item_title_details_text_view);
        mItemToolbar = (Toolbar) findViewById(R.id.item_detail_toolbar);
        mFab = (FloatingActionButton) findViewById(R.id.item_detail_fab);

        mItemToolbar.setTitle(selectedItem.getName());
        mItemToolbar.setBackgroundColor(selectedItem.getItemDescriptionBackgroundColor());
        setSupportActionBar(mItemToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mItemPrice = (TextView) findViewById(R.id.item_detail_price_text_view);
        mItemDescription = (TextView) findViewById(R.id.item_detail_description_text_view);
        mItemOrderPrice = (TextView) findViewById(R.id.item_detail_order_price_text_view);
        mItemOrderTotalPrice = (TextView) findViewById(R.id.total_item_price_order_text_view);
        mOrderButton = (Button) findViewById(R.id.item_detail_order_button);

        mOrderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Order order = new Order(selectedItem, orderAmount);
                OrderList.add(order);
                String message = getResources().getString(R.string.ui_message_orders_added);
                message = String.format(message, orderAmount, selectedItem.getName());
                Toast.makeText(ItemDetailsActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });

        mItemPrice.setText(selectedItem.getFormattedItemPrice());
        mItemDescription.setText(getResources().getText(selectedItem.getItemDescriptionId()));

        mItemOrderPrice.setText(selectedItem.getFormattedItemPrice());
        mItemOrderTotalPrice.setText(Items.toCurrencyFormat(selectedItem.getItemPrice() * orderAmount));

//        mOrderButton.setBackgroundResource(selectedItem.getItemDescriptionDarkMutedColor());

        mOrderRelativeLayout = (LinearLayout) findViewById(R.id.order_linear_layout);
        mOrderRelativeLayout.setBackgroundColor(selectedItem.getItemDescriptionDarkMutedColor());
        mOrderRelativeLayout.setVisibility(View.INVISIBLE);

        mItemDescriptionRelativeLayout = (RelativeLayout) findViewById(R.id.item_detail_linear_layout_bottom);


        Picasso.with(this).load(selectedItem.getItemImageId()).into(mItemImageView);
        mFab.setBackgroundTintList(ColorStateList.valueOf(selectedItem.getItemDescriptionVibrantColor()));
//        mItemTitleTextView.setText(selectedItem.getName());
//        mItemTitleTextView.setBackgroundColor(selectedItem.getItemDescriptionBackgroundColor());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mFab.setAlpha(0.0f);
            smoothEnterFab();
        }

        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int[] clickLocation = new int[2];
                v.getLocationOnScreen(clickLocation);

                clickLocation[0] += v.getWidth() / 2;
                clickLocation[1] += v.getHeight() / 2;

                int[] viewLocation = new int[2];
                mOrderRelativeLayout.getLocationOnScreen(viewLocation);

                int cx = clickLocation[0] - viewLocation[0];
                int cy = clickLocation[1] - viewLocation[1];

                Point size = new Point();
                getWindowManager().getDefaultDisplay().getSize(size);

                float finalRadius = size.y;
                Animator anim = null;
                if (!mOrderLayoutVisible) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        anim = ViewAnimationUtils.createCircularReveal(mOrderRelativeLayout, cx, cy, 0, finalRadius);
                        mOrderRelativeLayout.setVisibility(View.VISIBLE);
                        anim.start();
                    } else {
                        mOrderRelativeLayout.setVisibility(View.VISIBLE);
                        mItemDescriptionRelativeLayout.setVisibility(View.INVISIBLE);
                    }

                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        anim = ViewAnimationUtils.createCircularReveal(mOrderRelativeLayout, cx, cy, finalRadius, 0);

                        anim.addListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                mOrderRelativeLayout.setVisibility(View.INVISIBLE);
//                            mItemDescriptionRelativeLayout.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        });
                        anim.start();
                    } else {
                        mOrderRelativeLayout.setVisibility(View.INVISIBLE);
                        mItemDescriptionRelativeLayout.setVisibility(View.VISIBLE);

                    }
                }
                mOrderLayoutVisible = !mOrderLayoutVisible;
            }
        });
    }

    private void smoothEnterFab() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getEnterTransition().addListener(new Transition.TransitionListener() {
                @Override
                public void onTransitionStart(Transition transition) {

                }

                @Override
                public void onTransitionEnd(Transition transition) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        mFab.animate().alpha(1.0f);
                        getWindow().getEnterTransition().removeListener(this);
                    }
                }

                @Override
                public void onTransitionCancel(Transition transition) {

                }

                @Override
                public void onTransitionPause(Transition transition) {

                }

                @Override
                public void onTransitionResume(Transition transition) {

                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mFab = (FloatingActionButton) findViewById(R.id.item_detail_fab);
                mFab.setVisibility(View.INVISIBLE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    finishAfterTransition();
                } else {
                    finish();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void processOrder(View v) {
        TextView orderAmountView = (TextView) findViewById(R.id.order_amount_text_view);

        switch (v.getId()) {
            case R.id.order_add_amount_text_view:
                if (orderAmount < 9) {
                    orderAmount += 1;
                }
                break;
            case R.id.order_subtract_amount_text_view:
                if (orderAmount != 1) {
                    orderAmount -= 1;
                }
                break;
        }

        orderAmountView.setText(String.valueOf(orderAmount));

        mItemOrderTotalPrice = (TextView) findViewById(R.id.total_item_price_order_text_view);
        mItemOrderTotalPrice.setText(Items.toCurrencyFormat(selectedItem.getItemPrice() * orderAmount));

    }
}
