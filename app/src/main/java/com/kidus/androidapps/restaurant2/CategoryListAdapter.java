package com.kidus.androidapps.restaurant2;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by Kidus on .
 */
public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.ViewHolder> {

    Context mContext;
    Listener mListener;

    public interface Listener {
        void onClick(int Position, View view);
    }

    public void setListener(Listener listener) {
        this.mListener = listener;
    }

    public CategoryListAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_card_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Category category = Category.categories[position];
        holder.categoryName.setText(category.getCategoryNameString());
        Picasso.with(mContext).load(category.getCategoryImageResourceId()).into(holder.categoryImage);
        holder.categoryHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onClick(position, holder.categoryImage);
                }
            }
        });

        holder.categoryNameHolder.setBackgroundColor(Color.argb(96, 0, 0, 0));//won't use palette here :|

//        //palette :D
//        Bitmap photo = BitmapFactory.decodeResource(mContext.getResources(), category.getCategoryImageResourceId());
//        Palette.generateAsync(photo, new Palette.PaletteAsyncListener() {
//            @Override
//            public void onGenerated(Palette palette) {
//                int color = palette.getDarkMutedColor(mContext.getResources().getColor(R.color.dark_grey));
//                holder.categoryNameHolder.setBackgroundColor(color);
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return Category.categories.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CardView categoryCardView;
        public LinearLayout categoryHolder, categoryNameHolder;
        public TextView categoryName;
        public ImageView categoryImage;

        public ViewHolder(View itemView) {
            super(itemView);
            categoryCardView = (CardView) itemView.findViewById(R.id.category_card);
            categoryHolder = (LinearLayout) itemView.findViewById(R.id.category_cover_linear_layout);
            categoryNameHolder = (LinearLayout) itemView.findViewById(R.id.category_name_linear_layout);
            categoryName = (TextView) itemView.findViewById(R.id.category_name_text_view);
            categoryImage = (ImageView) itemView.findViewById(R.id.category_image_view);
        }
    }
}
