package com.kidus.androidapps.restaurant2;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by Kidus on .
 */
public class OrderListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Listener mListener;
    private Context mContext;
    private int mOrderTipPercentage;
    private Tip mTip;

    public OrderListAdapter(Context context) {
        mContext = context;
//        mTip = new Tip(OrderList.getTotalPrice(), 15/100.0);
    }

    public void setListener(Listener listener) {
        this.mListener = listener;
    }

    @Override
    public OrderListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) {
            View view = LayoutInflater.from(this.mContext).inflate(R.layout.order_items_card_view, parent, false);
            return new ViewHolder(view);
        } else {
            View view = LayoutInflater.from(this.mContext).inflate(R.layout.total_order_summary_card_view, parent, false);
            return new OrderSummaryViewHolder(view);
        }
    }

    @Override
    public int getItemViewType(int position) {
//        return super.getItemViewType(position);
        if (position < OrderList.size()) {
            return 0; //order card views
        } else {
            return 1; //summary card view
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder.getItemViewType() == 0) {//for the order cardviews
            ViewHolder viewHolder = (ViewHolder) holder;
            int orderAmount = OrderList.getOrder(position).getOrderAmount();
            double orderPrice = OrderList.getOrder(position).getOrderItemPrice();
            double totalPrice = orderPrice * orderAmount;
            viewHolder.mOrderTitle.setText(OrderList.getOrder(position).getOrderItemName());
            viewHolder.mOrderAmount.setText("Amount: " + String.valueOf(orderAmount));
            viewHolder.mOrderPrice.setText(Items.toCurrencyFormat(totalPrice));

            viewHolder.mCoverLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.onClick(position);
                    }
                }
            });

        } else {//for the total order summary cardview
            final OrderSummaryViewHolder orderSummaryViewHolder = (OrderSummaryViewHolder) holder;
            if (OrderList.isEmpty()) {
                orderSummaryViewHolder.mEmptyMessageLayout.setVisibility(View.VISIBLE);
            } else {
                orderSummaryViewHolder.mEmptyMessageLayout.setVisibility(View.INVISIBLE);
            }

            orderSummaryViewHolder.mTotalPriceTextView.setText(
                    String.format(orderSummaryViewHolder.mTotalLabel,
                            OrderList.getFormattedTotalPrice()));
//            mTip.setAmount(OrderList.getTotalPrice());
//            mTip.setPercentage(orderSummaryViewHolder.mProgressBar.getProgress() / 100.0);
            orderSummaryViewHolder.mProgressBar.setProgress(OrderList.getTipPercentageInt());
//            orderSummaryViewHolder.mProgressBar.setProgress(mTip.getPercentageInt());
            orderSummaryViewHolder.mCalculatedTipTextView.setText(
                    String.format(orderSummaryViewHolder.mTipAmountLabel,
                            Items.toCurrencyFormat(OrderList.getCalculatedTipAmount()))
            );
            orderSummaryViewHolder.mTotalPriceWithTipTextView.setText(
                    String.format(orderSummaryViewHolder.mTotalWithTipLabel,
                            Items.toCurrencyFormat(OrderList.getTotalAmountWithTip())));
            orderSummaryViewHolder.mPercentageValueTextView.setText(OrderList.getFormattedPercentage());
            orderSummaryViewHolder.mMinusTipTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (OrderList.getTipPercentage() >= 0.01) {
                        OrderList.decrementPercentage();
//                        OrderList.setTipPercentage(OrderList.getTipPercentage() - OrderList.getTipIncrementValue());
                        notifyDataSetChanged();
                    }
                }
            });
            orderSummaryViewHolder.mPlusTipTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (OrderList.getTipPercentage() <= 0.45) {
                        OrderList.incrementPercentage();
//                        OrderList.setTipPercentage(OrderList.getTipPercentage() + OrderList.getTipIncrementValue());
                        notifyDataSetChanged();
                    }
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return OrderList.size() + 1;
    }

    public void removeItem(int position) {
        OrderList.remove(position);
        OrderList.updateTip();
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, OrderList.size());
        notifyItemChanged(OrderList.size());//the last item i.e. the order summary

    }

    public interface Listener {
        void onClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CardView mCardView;
        private TextView mOrderTitle, mOrderPrice, mOrderAmount;
        private LinearLayout mCoverLinearLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            mCardView = (CardView) itemView.findViewById(R.id.order_item_card_view);
            mOrderTitle = (TextView) itemView.findViewById(R.id.order_item_title_text_view);
            mOrderAmount = (TextView) itemView.findViewById(R.id.order_amount_card_text_view);
            mOrderPrice = (TextView) itemView.findViewById(R.id.order_item_price_text_view);
            mCoverLinearLayout = (LinearLayout) itemView.findViewById(R.id.order_item_cover_linear_layout);
        }
    }

    public class OrderSummaryViewHolder extends ViewHolder {
        private CardView mOrderCardView;
        private TextView mTotalPriceTextView, mPercentageValueTextView, mCalculatedTipTextView, mTotalPriceWithTipTextView;
        private TextView mMinusTipTextView, mPlusTipTextView;
        private ProgressBar mProgressBar;
        private String mTotalLabel, mTipAmountLabel, mTotalWithTipLabel;
        private RelativeLayout mEmptyMessageLayout;

        public OrderSummaryViewHolder(View itemView) {
            super(itemView);
            mOrderCardView = (CardView) itemView.findViewById(R.id.total_order_summary_card_view);
            mTotalPriceTextView = (TextView) itemView.findViewById(R.id.total_order_summary_price_text_view);
            mPercentageValueTextView = (TextView) itemView.findViewById(R.id.total_order_tip_percentage_value_text_view);
            mCalculatedTipTextView = (TextView) itemView.findViewById(R.id.total_order_tip_calculated_text_view);
            mTotalPriceWithTipTextView = (TextView) itemView.findViewById(R.id.total_order_total_price_with_tip_text_view);
            mMinusTipTextView = (TextView) itemView.findViewById(R.id.total_order_minus_label_button_text_view);
            mPlusTipTextView = (TextView) itemView.findViewById(R.id.total_order_plus_label_button_text_view);
            mProgressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
            mEmptyMessageLayout = (RelativeLayout) itemView.findViewById(R.id.total_order_summary_empty_relative_layout);
            mTotalLabel = itemView.getResources().getString(R.string.ui_message_order_summary_total);
            mTipAmountLabel = itemView.getResources().getString(R.string.ui_message_order_summary_calculated_tip);
            mTotalWithTipLabel = itemView.getResources().getString(R.string.ui_message_order_summary_total_with_tip);
        }
    }
}
