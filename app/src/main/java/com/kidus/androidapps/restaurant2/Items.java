package com.kidus.androidapps.restaurant2;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.graphics.Palette;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.UUID;

/**
 * Created by Kidus on .
 */
public class Items implements Serializable {
    private UUID mItemId;
    private String mName;
    private int mItemImageId;
    private int mItemThumbnailImageId;
    //private Category mItemCategory;
    private double mItemPrice;
    private int mItemDescriptionId;
    private int mItemDescriptionBackgroundColor;
    private int mItemDescriptionForegroundColor;
    private int mItemDescriptionVibrantColor;
    private int mItemDescriptionDarkMutedColor;
    private String mFormattedItemPrice;

    public static final Items[] pizzaItems = {
            initItems(getStringFromResource(R.string.cali_pizza_name), R.drawable.piz_california, 199.95, R.string.cali_pizza_details),
            initItems(getStringFromResource(R.string.meat_pizza_name), R.drawable.piz_meat_lovers, 176.35, R.string.meat_pizza_details),
            initItems(getStringFromResource(R.string.chicken_pizza_name), R.drawable.piz_chicken_pizza, 145.00, R.string.chicken_pizza_details),
            initItems(getStringFromResource(R.string.chicago_pizza_name), R.drawable.piz_chicago, 155.55, R.string.chicago_pizza_details),
            initItems(getStringFromResource(R.string.chicago_thin_pizza_name), R.drawable.piz_chicago_thin_crust,150.00, R.string.chicago_thin_pizza_details),
            initItems(getStringFromResource(R.string.neapolitan_pizza_name), R.drawable.piz_neapolitan, 121.00,R.string.neapolitan_pizza_details),
            initItems(getStringFromResource(R.string.greek_pizza_name), R.drawable.piz_greek_style, 133.20, R.string.greek_pizza_details),
            initItems(getStringFromResource(R.string.detroit_pizza_name), R.drawable.piz_detroit_style, 165.75, R.string.detroit_pizza_details),
    };

    public static final Items[] burgerItems = {
            initItems(getStringFromResource(R.string.cheese_burger_name), R.drawable.bur_cheeseburger, 95.00, R.string.cheese_burger_details),
            initItems(getStringFromResource(R.string.juicy_burger_name), R.drawable.bur_juicy_lucy_burger, 105.95, R.string.juicy_burger_details),
            initItems(getStringFromResource(R.string.steak_burger_name), R.drawable.bur_steak_burger, 121.25, R.string.steak_burger_details),
            initItems(getStringFromResource(R.string.veggie_burger_name), R.drawable.bur_veggie_burger, 75.25, R.string.veggie_burger_details),
    };

    public static final Items[] softDrinkItems = {
            initItems(getStringFromResource(R.string.coca_soft_name), R.drawable.sof_coca, 20.00, R.string.coca_soft_details),
            initItems(getStringFromResource(R.string.fanta_soft_name), R.drawable.sof_fanta, 20.00, R.string.fanta_soft_details),
            initItems(getStringFromResource(R.string.sprite_soft_name), R.drawable.sof_sprite, 20.00, R.string.sprite_soft_details),
            initItems(getStringFromResource(R.string.pepsi_soft_name), R.drawable.sof_pepsi, 20.00, R.string.pepsi_soft_details),
            initItems(getStringFromResource(R.string.mirinda_soft_name), R.drawable.sof_mirinda, 20.00, R.string.mirinda_soft_details),
            initItems(getStringFromResource(R.string.seven_soft_name), R.drawable.sof_seven_up, 20.00, R.string.seven_soft_details),
    };

    public static final Items[] alcoholicDrinkItems = {
            initItems(getStringFromResource(R.string.walia_alc_name), R.drawable.w_alc_img_walia_wide, 25.00, R.string.walia_alc_details),
            initItems(getStringFromResource(R.string.heineken_alc_name), R.drawable.w_alc_img_hein, 65.00, R.string.heineken_alc_details),
            initItems(getStringFromResource(R.string.budweiser_alc_name), R.drawable.w_alc_img_bud, 65.00, R.string.budweiser_alc_details),
            initItems(getStringFromResource(R.string.jack_alc_name), R.drawable.alc_jack, 1100.00, R.string.jack_alc_details),
            initItems(getStringFromResource(R.string.absolut_alc_name), R.drawable.w_alc_img_absolut, 900.00, R.string.absolut_alc_details),
            initItems(getStringFromResource(R.string.grey_alc_name), R.drawable.w_alc_img_grey_goose, 1200.00, R.string.grey_alc_details),
    };

    public static final Items[] ethiopianFoodItems = {
            initItems(getStringFromResource(R.string.doro_eth_name), R.drawable.eth_doro, 180.25, R.string.doro_eth_details),
            initItems(getStringFromResource(R.string.kitfo_eth_name), R.drawable.eth_kitfo, 165.95, R.string.kitfo_eth_details),
            initItems(getStringFromResource(R.string.misir_eth_name), R.drawable.eth_misir_wot, 75.50, R.string.misir_eth_details),
            initItems(getStringFromResource(R.string.tibs_eth_name), R.drawable.eth_tibs, 95.00, R.string.tibs_eth_details),
    };

    public static Items[] spaghettiItems = {
            initItems(getStringFromResource(R.string.spaghetti_alla_puttanesca_name), R.drawable.spaghetti_alla_puttanesca, 95.00, R.string.spaghetti_alla_puttanesca_details),
            initItems(getStringFromResource(R.string.carbonara_name),R.drawable.carbonara, 84.00, R.string.carbonara_details),
            initItems(getStringFromResource(R.string.spaghetti_and_meatballs_name), R.drawable.spaghetti_and_meatballs, 90.00, R.string.spaghetti_and_meatballs_details),
            initItems(getStringFromResource(R.string.spaghetti_aglio_e_olio_name), R.drawable.spaghetti_aglio_e_olio, 105.25, R.string.spaghetti_aglio_e_olio_details)
    };

    public int getItemDescriptionBackgroundColor() {
        return mItemDescriptionBackgroundColor;
    }

    public Items(String name, int itemImageId, double itemPrice, int itemDescriptionId,
                 int itemDescriptionBackgroundColor, int itemDescriptionVibrantColor,
                 int itemDescriptionDarkMutedColor,
                 String formattedItemPrice) {
        this.mItemId = UUID.randomUUID();
        this.mName = name;
        this.mItemImageId = itemImageId;
        //this.mItemCategory = itemCategory;
        this.mItemPrice = itemPrice;
        this.mItemDescriptionId = itemDescriptionId;
        this.mItemDescriptionBackgroundColor = itemDescriptionBackgroundColor;
        this.mFormattedItemPrice = formattedItemPrice;
        this.mItemDescriptionVibrantColor = itemDescriptionVibrantColor;
        this.mItemDescriptionDarkMutedColor = itemDescriptionDarkMutedColor;
    }

    public UUID getItemId() {
        return mItemId;
    }

    public double getItemPrice() {
        return mItemPrice;
    }

    public int getItemDescriptionId() {
        return  mItemDescriptionId;

    }

    public String getName() {
        return mName;
    }



    public int getItemImageId() {
        return mItemImageId;
    }

    private static String getStringFromResource(int id) {
        return MainActivity.getContext().getResources().getString(id);
    }

    private static int getDarkVibrantColor(int resourceID) {
        Bitmap bitmap = ((BitmapDrawable) MainActivity.getContext().getResources().getDrawable(resourceID)).getBitmap();
        return Palette.generate(bitmap).getDarkVibrantColor(Color.argb(255, 63, 81, 181));
    }

    private static int getVibrantColor(int resourceID) {
        Bitmap bitmap = ((BitmapDrawable) MainActivity.getContext().getResources().getDrawable(resourceID)).getBitmap();
        return Palette.generate(bitmap).getVibrantColor(Color.argb(255, 63, 81, 181));
    }

    private static int getDarkMutedColor(int resourceID) {
        Bitmap bitmap = ((BitmapDrawable) MainActivity.getContext().getResources().getDrawable(resourceID)).getBitmap();
        return Palette.generate(bitmap).getDarkMutedColor(Color.argb(255, 84, 96, 96));
    }

    public String getFormattedItemPrice() {
        return mFormattedItemPrice;
    }

    public int getItemDescriptionVibrantColor() {
        return mItemDescriptionVibrantColor;
    }

    private static Items initItems(String name, int itemImageId, double itemPrice, int itemDescriptionId) {
        return new Items (name, itemImageId, itemPrice, itemDescriptionId,
                getDarkVibrantColor(itemImageId), getVibrantColor(itemImageId),
                getDarkMutedColor(itemImageId),
                toCurrencyFormat(itemPrice));
    }

    public static String toCurrencyFormat(double d) {
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance();
        return numberFormat.format(d);
    }

    public int getItemDescriptionDarkMutedColor() {
        return mItemDescriptionDarkMutedColor;
    }
    //    public Category getItemCategory() {
//        return mItemCategory;
//    }
}
