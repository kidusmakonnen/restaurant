package com.kidus.androidapps.restaurant2;

import android.content.Intent;
import android.media.Image;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class TestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
    }

    public void goaway(View view) {

        Intent intent = new Intent(this, ItemListActivity.class);
        Pair<View, String> cat = Pair.create(view, "transition_category_image");
        ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this,
                cat
        );
        ActivityCompat.startActivity(this, intent, optionsCompat.toBundle());

    }
}
